#!/bin/bash

#Leatherface, the Ferrage's filenames parsing tool for his paintings

#author : electropositron
#version : 0.1
#licence : GPL

#on se rend dans le répertoire courant
cd ${1}

#supprime un tableau de répertoire, s'il existe
if test -f repertoire_oeuvres.csv
    then
        rm repertoire_oeuvres.csv
    fi
#crée un tableau CSV avec les bon headers
echo "date de début du carnet,date de fin du carnet,numéro de la page,titre de l'oeuvre,hauteur de l'image(cm),largeur de l'image(cm),série,tag,technique,avancement,avis de Marc,statut du scan,version du dessin,prix,en ligne ?,sur la boutique ?" >> repertoire_oeuvres.csv

#boucle de lecture des fichiers présent dans le répertoire et les sous-répertoires avec le signe "_" et une extension .png
for name in */*.png ;
    do
     
#chope toutes les données de l'image courante
        date_debut=$(echo "${name}" | awk -F '_' '{print $1}')
        date_fin=$(echo "${name}" | awk -F '_' '{print $2}')
        num_page=$(echo "${name}" | awk -F '_' '{print $3}')
        titre=$(echo "${name}" | awk -F '_' '{print $4}')
        hauteur=$(echo "${name}" | awk -F '_' '{print $5}')
        largeur=$(echo "${name}" | awk -F '_' '{print $6}')
        serie=$(echo "${name}" | awk -F '_' '{print $7}')
        tag=$(echo "${name}" | awk -F '_' '{print $8}')
        technique=$(echo "${name}" | awk -F '_' '{print $9}')
        avancement=$(echo "${name}" | awk -F '_' '{print $10}')
        avis_marc=$(echo "${name}" | awk -F '_' '{print $11}')
        scan_status=$(echo "${name}" | awk -F '_' '{print $12}')
        version_dessin=$(echo "${name}" | awk -F '_' '{print $13}')
        prix=$(echo "${name}" | awk -F '_' '{print $14}')
        site_internet=$(echo "${name}" | awk -F '_' '{print $15}')
        boutique=$(echo "${name}" | awk -F '_' '{print $16}')
# écrit les informations correspondant à une ligne du CSV
        echo ${date_debut}","${date_fin}","${num_page}","${titre}","${hauteur}","${largeur}","${serie}","${tag}","${technique}","${avancement}","${avis_marc}","${scan_status}","${version_dessin}","${prix}","${site_internet}","${boutique} >> repertoire_oeuvres.csv
    done