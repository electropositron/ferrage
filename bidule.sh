#!/bin/bash

#Bidule, the Ferrage's nomenclature tool for his paintings

#author : electropositron
#version : 0.1
#licence : GPL

cd $1
echo on travaille dans le répertoire ${1}

#-----INPUTS-----
date_debut=$(zenity --calendar --text="Date de début du carnet" --date-format=%y%m%d)

date_fin=$(zenity --calendar --text="Date de fin du carnet" --date-format=%y%m%d)

num_page=$(zenity --entry --text="Numéro de page")

titre=$(zenity --entry --text="Titre de l'oeuvre")

hauteur=$(zenity --entry --text="Hauteur en cm")

largeur=$(zenity --entry --text="Largeur en cm")

serie=$(zenity --entry --text="Nom de la série de dessins")

tag=$(zenity --entry --text="Tags (séparés par des -)")

technique=$(zenity --entry --text="Techniques (séparées par des -)")

avancement=$(zenity --list --text="avancement de l'oeuvre" --column="avancement" "fini" "quasi fini" "pas fini")

avis_marc=$(zenity --list --text="Avis Marc" --column="avis" "Bien" "Bien -" "Moyen +" "Moyen -" "à retravailler")

scan_status=$(zenity --list --text="Scan" --column="resolution" "72 DPI web " "300 DPI" "600 DPI" "raté, à rescanner")

version_dessin="V"
version_dessin+=$(zenity --entry --text="Version du dessin (format 01,02,etc)")

prix=$(zenity --entry --text="Prix")

site_internet=$(zenity --list --text="Montré sur le site ?" --column="online" "oui" "non")

boutique=$(zenity --list --text="Vendu sur la boutique ?" --column="boutique" "oui" "non")

format_fichier="."
format_fichier+=${2#*.}

#-----RENAME FILE-----

#creation du nom du fichier par concatenation des variables

file_name=${date_debut}"_"${date_fin}"_"${num_page}"_"${titre}"_"${hauteur}"_"${largeur}"_"${serie}"_"${tag}"_"${technique}"_"${avancement}"_"${avis_marc}"_"${scan_status}"_"${version_dessin}"_"${prix}"_"${site_internet}"_"${boutique}"_"${format_fichier}

#rename current file

mv "${2}" "${file_name}"